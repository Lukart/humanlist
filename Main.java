package human;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static List<Human> people = new ArrayList<>();

    private static void main(String[] args) {
        menu();
    }

    private static void menu() {
        Scanner scanner = new Scanner(System.in);
        int menuButton;
        do {
            printMenu();
            menuButton = scanner.nextInt(5);
            switch (menuButton) {
                case 1:
                    addHumman(scanner);
                    break;
                case 2:
                    deleteHumman(scanner);
                    break;
                case 3:
                    compareHumman(people, scanner);
                    break;
                case 4:
                    showHummans();
                    break;
            }
        }
        while (menuButton != 0);
    }

    private static void printMenu() {
        System.out.println();
        System.out.println("[MENU] Set[0-4]:");
        System.out.println("[1] = Add human");
        System.out.println("[2] = Delete human");
        System.out.println("[3] = Compare human");
        System.out.println("[4] = Show humans");
        System.out.println("[0] = EXIT");
    }

    private static void addHumman(Scanner scanner) {
        System.out.println("Add human first name:");
        String name = scanner.next();
        System.out.println("Add human surname:");
        String surname = scanner.next();
        System.out.println("Add human age:");
        int age = scanner.nextInt();
        Human humans = new Human(name, surname, age);
        people.add(humans);
    }

    private static void deleteHumman(Scanner scanner) {
        System.out.println("Set index of human to remove:");
        int removeIndex = scanner.nextInt();
        people.remove(removeIndex);
    }

    private static void compareHumman(List<Human> people, Scanner scanner) {
        System.out.println("Set index of first human to compare:");
        int indexHuman1 = scanner.nextInt();
        System.out.println("Set index of second human to compare:");
        int indexHuman2 = scanner.nextInt();
        Human human1 = people.get(indexHuman1);
        Human human2 = people.get(indexHuman2);
        System.out.println("Human[" + indexHuman1 + "] is equals Human[" + indexHuman2 + "] ==> " + human1.equals
                (human2));
    }

    private static void showHummans() {
        for (int i = 0; i < people.size(); i++) {
            System.out.println("[index: " + (i) + "] ==> " + "[" + people.get(i) + "]");
        }
        System.out.println("How many humans? ==> " + people.size() + "\n");
    }
}
